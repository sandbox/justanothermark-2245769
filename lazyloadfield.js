(function ($, Drupal) {
  Drupal.behaviors.lazyLoadField = {
    attach: function () {
      $(".lazyloadfield-ajax-include")
        .ajaxInclude()
        // Use bind() instead of on() for compatibility with pre 1.7 jQuery.
        .bind('ajaxIncludeResponse', function() {
          Drupal.attachBehaviors();
        });
    }
  };
})(jQuery, Drupal);
